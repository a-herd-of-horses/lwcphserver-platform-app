import request from "@/utils/request.js";
import Cache from "@/utils/cache.js"
/*
 * 校验合伙人
 * */
export function verifyPartners(id) {
	return request.get("grade/checkPartnerCondition/" + id);
}
/*
 * 申请成为合伙人
 * */
export function applyPartners(data) {
	return request.post(
		`grade/applyPartner?applicantLevel=${data.applicantLevel}&areaCode=${data.areaCode}`,
		// data,
	);
}